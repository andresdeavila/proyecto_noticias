require 'rest-client'
require 'json'
require 'colorize'

class MashableNews

  attr_accessor :title, :author, :date, :url, :origin
  attr_reader :reddit_db
  MARSHABLE_URL = 'http://mashable.com/stories.json'
  @@mashable_db = []

  def initialize(origin, title, author, date, url)
    @origin = origin
    @title = title
    @author = author
    @date = date
    @url = url
  end

  def self.marshable_create_db
    news_info = RestClient.get(MARSHABLE_URL)
    news_info = JSON.parse news_info.body

    news_info['new'].each do |val|
        @@mashable_db << MashableNews.new('Mashable', val['title'], val['author'], Time.parse(val['post_date']).to_i, val['link'])
    end
    @@mashable_db.sort_by! { |k| k.date}
    @@mashable_db.reverse!
    marshable_show_news
  end

  def self.marshable_show_news
    @@mashable_db.map do |val| 
      puts %{
|---------------------------------------- O ----------------------------------------|




  Origin: #{val.origin.colorize(:cyan)}

  Title: #{val.title}

  Author: #{val.author}

  Date: #{Time.at(val.date).strftime('%d/%m/%Y')}

  URL: #{val.url.colorize(:blue)}


      }
    end
    puts "Presione enter para continuar"
    x = gets.chomp
  end

end
require 'rest-client'
require 'json'
require 'colorize'

class DiggNews

  attr_accessor :title, :author, :date, :url, :origin
  attr_reader :reddit_db
  DIGG_URL = 'http://digg.com/api/news/popular.json'
  @@digg_db = []

  def initialize(origin, title, author, date, url)
    @origin = origin
    @title = title
    @author = author
    @date = date
    @url = url
  end

  def self.digg_create_db
    news_info = RestClient.get(DIGG_URL)
    news_info = JSON.parse news_info.body

    news_info['data']['feed'].each do |val|
      @@digg_db << DiggNews.new('Digg', val['content']['title_alt'], val['content']['author'], val['date'], val['content']['original_url'])
    end
    @@digg_db.sort_by! { |k| k.date}
    @@digg_db.reverse!
    reddit_show_news
  end

  def self.reddit_show_news
    @@digg_db.map do |val| 
      puts %{
|---------------------------------------- O ----------------------------------------|




  Origin: #{val.origin.colorize(:green)}

  Title: #{val.title}

  Author: #{val.author}

  Date: #{Time.at(val.date).strftime('%d/%m/%Y')}

  URL: #{val.url.colorize(:blue)}


      }
    end
    puts "Presione enter para continuar"
    x = gets.chomp
  end

end
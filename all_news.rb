require 'rest-client'
require 'json'
require 'colorize'

class AllNews

  attr_accessor :title, :author, :date, :url, :origin
  attr_reader :reddit_db
  REDDIT_URL = 'https://www.reddit.com/.json'
  MARSHABLE_URL = 'http://mashable.com/stories.json'  
  DIGG_URL = 'http://digg.com/api/news/popular.json'
  @@all_db = []

  def initialize(origin, title, author, date, url)
    @origin = origin
    @title = title
    @author = author
    @date = date
    @url = url
  end

  def self.all_create_db

    news_info = RestClient.get(REDDIT_URL)
    news_info = JSON.parse news_info.body
    news_info['data']['children'].each do |val|
      @@all_db << AllNews.new('Reddit', val['data']['title'], val['data']['author'], val['data']['created'].to_i, val['data']['url'])
    end

    news_info = RestClient.get(MARSHABLE_URL)
    news_info = JSON.parse news_info.body
    news_info['new'].each do |val|
      @@all_db << AllNews.new('Mashable', val['title'], val['author'], Time.parse(val['post_date']).to_i, val['link'])
    end

    news_info = RestClient.get(DIGG_URL)
    news_info = JSON.parse news_info.body
    news_info['data']['feed'].each do |val|
      @@all_db << AllNews.new('Digg', val['content']['title_alt'], val['content']['author'], val['date'].to_i, val['content']['original_url'])
    end
    @@all_db.sort_by! { |k| k.date}
    @@all_db.reverse!
    all_show_news
  end

  def self.all_show_news
    @@all_db.map do |val| 
      puts %{
|---------------------------------------- O ----------------------------------------|




  Origin: #{val.origin.colorize(:blue)}

  Title: #{val.title}

  Author: #{val.author}

  Date: #{Time.at(val.date).strftime('%d/%m/%Y')}

  URL: #{val.url.colorize(:blue)}


      }
    end
    puts "Presione enter para continuar"
    x = gets.chomp
  end

end
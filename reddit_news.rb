require 'rest-client'
require 'json'
require 'colorize'

class RedditNews

  attr_accessor :title, :author, :date, :url, :origin
  attr_reader :reddit_db
  REDDIT_URL = 'https://www.reddit.com/.json'
  @@reddit_db = []

  def initialize(origin, title, author, date, url)
    @origin = origin
    @title = title
    @author = author
    @date = date
    @url = url
  end

  def self.reddit_create_db
    news_info = RestClient.get(REDDIT_URL)
    news_info = JSON.parse news_info.body

    news_info['data']['children'].each do |val|
        @@reddit_db << RedditNews.new('Reddit', val['data']['title'], val['data']['author'], val['data']['created'], val['data']['url'])
    end
    @@reddit_db.sort_by! { |k| k.date}
    @@reddit_db.reverse!
    reddit_show_news
  end

  def self.reddit_show_news
    @@reddit_db.map do |val| 
      puts %{
|---------------------------------------- O ----------------------------------------|




  Origin: #{val.origin.colorize(:yellow)}

  Title: #{val.title}

  Author: #{val.author}

  Date: #{Time.at(val.date).strftime('%d/%m/%Y')}

  URL: #{val.url.colorize(:blue)}


    }
    end
    puts "Presione enter para continuar"
    x = gets.chomp
  end

end
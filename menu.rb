require_relative 'reddit_news'
require_relative 'mashable_news'
require_relative 'digg_news'
require_relative 'all_news'
require 'colorize'

class Menu

  def initialize
    main_menu
  end

  def main_menu
    loop do
    system('clear')    
    puts %{

  #{'Welcome to Lykos News'.colorize(:red)}

  Please pick an option:

    1) Reddit News
    2) Mashable News
    3) Digg News
    4) All News
    5) Exit
    }

    case x = gets.strip
    when '1'
      RedditNews.reddit_create_db
    when '2'
      MashableNews.marshable_create_db
    when '3'
      DiggNews.digg_create_db
    when '4' 
      AllNews.all_create_db
    when '5'
      finish = true 
      system('clear')
    else
      system('clear')          
      main_menu
    end

    break if finish == true
    end
  end

end


